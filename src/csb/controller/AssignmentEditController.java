package csb.controller;

import static csb.CSB_PropertyType.REMOVE_ITEM_MESSAGE;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.Assignment;
import csb.gui.CSB_GUI;
import csb.gui.MessageDialog;
import csb.gui.AssignmentDialog;
import csb.gui.YesNoCancelDialog;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
/**
 *
 * @author Awaes
 */
public class AssignmentEditController {
    AssignmentDialog aid;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    public AssignmentEditController(Stage initPrimaryStage, Course course, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        aid = new AssignmentDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    // THESE ARE FOR ASSIGNMENTS
    
    public void handleAddAssignmentRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        aid.showAddAssignmentDialog(course.getStartingMonday());
        
        // DID THE USER CONFIRM?
        if (aid.wasCompleteSelected()) {
            // GET THE ASSIGNMENT
            Assignment ai = aid.getAssignment();
            
            // AND ADD IT AS A ROW TO THE TABLE
            course.addAssignment(ai);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleEditAssignmentRequest(CSB_GUI gui, Assignment itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        aid.showEditAssignmentDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (aid.wasCompleteSelected()) {
            // UPDATE THE ASSIGNMENT
            Assignment ai = aid.getAssignment();
            itemToEdit.setName(ai.getName());
            itemToEdit.setDate(ai.getDate());
            itemToEdit.setTopics(ai.getTopics());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    
    public void handleRemoveAssignmentRequest(CSB_GUI gui, Assignment itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeAssignment(itemToRemove);
        }
    }
}

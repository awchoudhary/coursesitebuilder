package csb.gui;
import csb.data.Course;
import java.util.concurrent.locks.ReentrantLock;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.Modality;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;


/**
 *
 * @author Awaes
 */
public class ProgressDialog extends Stage {
    ProgressBar bar;
    ProgressIndicator indicator;
    Label processLabel;
    int numTasks = 0;
    ReentrantLock progressLock;
    
    public ProgressDialog(Course course){
        /*initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);*/
        progressLock = new ReentrantLock();
        VBox box = new VBox();

        HBox toolbar = new HBox();
        bar = new ProgressBar(0);  
        bar.getStyleClass().add("progress_bar");
        indicator = new ProgressIndicator(0);
        indicator.getStyleClass().add("progress_pie");
        toolbar.getChildren().add(bar);
        toolbar.getChildren().add(indicator);
        
        processLabel = new Label();
        processLabel.setFont(new Font("Serif", 36));
        box.getChildren().add(toolbar);
        box.getChildren().add(processLabel);
        
        Scene scene = new Scene(box);
        scene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(scene);

                Task<Void> task = new Task<Void>() {
                    int task = numTasks++;
                    double max = course.getPages().size() * 100;
                    double perc;
                    @Override
                    protected Void call() throws Exception {
                        try {
                            progressLock.lock();
                        for (int i = 0; i <= max; i+=max/(course.getPages().size())) {
                            perc = i/max;
                            
                            // THIS WILL BE DONE ASYNCHRONOUSLY VIA MULTITHREADING
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    bar.setProgress(perc);
                                    indicator.setProgress(perc);
                                    
                                }
                            });

                            // SLEEP EACH FRAME
                            //try {
                                Thread.sleep(1000);
                                
                           // } catch (InterruptedException ie) {
                          //      ie.printStackTrace();
                          //  }
                        }}
                        finally {
                                progressLock.unlock();
                                }
                        return null;
                    }
                };
                // THIS GETS THE THREAD ROLLING
                Thread thread = new Thread(task);
                thread.start();            
        
    }
    
    public void showProgressDialog() {
        this.showAndWait();
    }
}
